# Base image
FROM node:18

# Set working directory in the container
WORKDIR /app

# Copy package.json and package-lock.json to the working directory
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of your app's source code from your host to your image filesystem.
COPY . .

# Expose port on the Docker container
EXPOSE 3000

# Start the application
CMD ["npm", "start"]

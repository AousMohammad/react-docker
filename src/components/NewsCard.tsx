import * as React from 'react';
import { Datum } from 'types';
import { Card, Box, Skeleton, useMediaQuery, useTheme, Typography, CardMedia, CardContent } from '@mui/material';
import { NewsCardMobile } from './NewsCardMobile';
import { Link } from 'react-router-dom';

type Props = {
    data: Datum[] | undefined;
    loading: boolean;
}
export const NewsCard: React.FC<Props> = ({ data, loading }) => {
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.up('md'));

    if (matches) {
        return (
            <>
                {
                    data?.map((item) =>
                        <Card sx={{ display: 'flex', my: 2, height: 200 }} key={item.title}>
                            {loading ? (
                                <Skeleton variant="rectangular" sx={{ width: 151, flex: '2' }} height='100%' />
                            ) : (
                                <CardMedia
                                    component="img"
                                    sx={{ width: 151, flex: '2' }}
                                    image={item.image_url ?? 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSKteJt4yIuQOY8dv1-oknm_gx4IRimwZ_rzOT0LY7NiFnd5JsBD8ofiiPhMGeaYpXEr-0&usqp=CAU'}
                                    alt="Live from space album cover"
                                />
                            )}
                            <Box sx={{ display: 'flex', flex: '7', flexDirection: 'column' }}>
                                <CardContent sx={{ flex: '1 0 auto' }}>
                                    {loading ? (<>
                                        <Skeleton variant="text" sx={{ fontSize: '1rem' }} />
                                    </>) : (
                                        <Typography component="div" variant="h5">
                                            {item.title}
                                        </Typography>
                                    )}
                                    {loading ? (<>
                                        <Skeleton variant="text" sx={{ fontSize: '2rem' }} />
                                    </>) : (
                                        <Typography variant="subtitle1" color="text.secondary" component="div">
                                            {item.description}
                                        </Typography>
                                    )}
                                </CardContent>
                            </Box>
                            <Box display='flex' flex='1' alignItems='center'>
                                {loading ? (
                                    <Skeleton variant="text" sx={{ width: '50%', fontSize: '1rem' }} />
                                ) : (
                                    <Link to={'/article/' + item.id}>
                                        Read more
                                    </Link>
                                )}
                            </Box>
                        </Card >
                    )
                }
            </>
        );
    } else {
        return (
            <>
                {
                    data?.map((item) =>
                        <NewsCardMobile key={item.title} item={item} loading={loading} />
                    )
                }
            </>
        )
    }
}




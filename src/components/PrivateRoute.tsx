import React, { useEffect, useLayoutEffect } from 'react';
import { useAuth } from 'hooks';
import { Outlet, useNavigate } from 'react-router-dom';
import Layout from './Layout';
import { Backdrop, CircularProgress } from '@mui/material';

const PrivateRoute: React.FC = () => {
  const { auth, loading } = useAuth();
  const navigate = useNavigate();
  useLayoutEffect(() => {
    if (loading) {
      return;
    }
    if (auth === false) {
      navigate('/login', { replace: true });
    }
  }, [auth, loading, navigate]);

  if (loading) {
    return <Backdrop
      sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
      open={true}
    >
      <CircularProgress color="inherit" />
    </Backdrop>
  }

  return (
    <Layout>
      <Outlet />
    </Layout>
  );
};

export default PrivateRoute;

import React from 'react'
import { Avatar, Card, CardActions, CardContent, CardHeader, CardMedia, Collapse, IconButton, IconButtonProps, Skeleton, Typography, styled } from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
type Props = {
    loading: boolean;
    item: any;
}

export const NewsCardMobile: React.FC<Props> = ({ loading, item }) => {
    const [expanded, setExpanded] = React.useState(false);

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };
    return (
        <Card key={item.title} sx={{ width: '100%', mb: 3, boxShadow: '0px 0px 11px 0px #00000057', bg: '#80808017' }}>
            {loading ? (
                <Skeleton variant="circular" height={50} width={50} />
            ) : (
                <CardHeader
                    avatar={
                        <Avatar sx={{ bgcolor: 'red' }} aria-label="recipe">
                            {item?.author?.name[0]}
                        </Avatar>
                    }
                    title={item?.author?.name}
                    subheader={`Published at: ${item.published_at.toString()}`}
                />
            )}
            {loading ? (
                <Skeleton variant="rectangular" height={194} />
            ) : (
                <CardMedia
                    component="img"
                    height="194"
                    image={item.image_url ?? 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSKteJt4yIuQOY8dv1-oknm_gx4IRimwZ_rzOT0LY7NiFnd5JsBD8ofiiPhMGeaYpXEr-0&usqp=CAU'}
                    alt="Paella dish"
                />
            )}
            <CardContent>
                <Typography variant="body2" color="text.secondary">
                    Author: {item.author?.name}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    Source: {item.source.name}
                </Typography>
            </CardContent>
            <CardActions disableSpacing>
                <ExpandMore
                    expand={expanded}
                    onClick={handleExpandClick}
                    aria-expanded={expanded}
                    aria-label="show more"
                >
                    <ExpandMoreIcon />
                </ExpandMore>
                See more
            </CardActions>
            <Collapse in={expanded} timeout="auto" unmountOnExit>
                <CardContent>
                    <Typography>
                        {item.title}
                    </Typography>
                    <Typography paragraph>
                        {item.description}
                    </Typography>
                </CardContent>
            </Collapse>
        </Card>
    )
}


interface ExpandMoreProps extends IconButtonProps {
    expand: boolean;
}

const ExpandMore = styled((props: ExpandMoreProps) => {
    const { expand, ...other } = props;
    return <IconButton {...other} />;
})(({ theme, expand }) => ({
    transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
    }),
}));
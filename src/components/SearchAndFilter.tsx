import React, { useRef } from "react";
import { DateSearch, SourceSearch, AuthorSearch } from "./Filter";
import { Button, Grid } from "@mui/material";
import { useDispatch } from "react-redux";
import { setAuthorSearch, setDateSearch, setSourceSearch } from "redux/actions";

export type Filters = {
    author: string;
    date: Date | null;
    source: string;
};

export const SearchAndFilter: React.FC = () => {
    const dispatch = useDispatch();
    const authorRef = useRef<any>(null);
    const dateRef = useRef<any>(null);
    const sourceRef = useRef<any>(null);

    const resetFilters = () => {
        // Clear the filter values in your state
        dispatch(setAuthorSearch(""));
        dispatch(setDateSearch(null));
        dispatch(setSourceSearch(""));
    };

    function convertDateFormat(inputDate: string) {
        // Split the input date string by the "-" delimiter
        const parts = inputDate.split('-');

        // Check if the input date has three parts (MONTH, DAY, YEAR)
        if (parts.length === 3) {
            // Rearrange the parts to form the new date format (YEAR, MONTH, DAY)
            const [month, day, year] = parts;
            const newDateFormat = `${year}-${month}-${day}`;
            return newDateFormat;
        } else {
            // Invalid input format
            return null;
        }
    }

    const ApplyFilter = (event: any) => {
        event.preventDefault();
        const authorSearchData = authorRef.current?.getData();
        const temoData = dateRef.current?.getData();
        const dateData = new Date(formatDate(temoData));
        const sourceData = sourceRef.current?.getData();
        // Create an object with action functions
        const actionsToDispatch = {
            setAuthorSearch: () => dispatch(setAuthorSearch(authorSearchData)),
            setDateSearch: () => dispatch(setDateSearch(convertDateFormat(dateData.toLocaleDateString().replaceAll("/", "-")))),
            setSourceSearch: () => dispatch(setSourceSearch(sourceData)),
        };

        // Dispatch all actions using a single dispatch call
        Object.values(actionsToDispatch).forEach((actionFunction) =>
            actionFunction()
        );
    };




    // Helper function to format a Date object as "day/month/year"
    const formatDate = (date: any) => {
        if (date instanceof Date) {
            return date.toLocaleDateString(undefined, {
                day: "numeric",
                month: "numeric",
                year: "numeric",
            });
        }
        return "";
    };

    return (
        <form style={{ width: "100%" }} onSubmit={ApplyFilter}>
            <Grid
                container
                spacing={3}
                sx={{
                    padding: "1rem",
                    borderRadius: "8px",
                }}
            >
                <Grid item xs={12} md={3}>
                    <AuthorSearch ref={authorRef} />
                </Grid>
                <Grid item xs={12} md={3}>
                    <DateSearch ref={dateRef} />
                </Grid>
                <Grid item xs={12} md={3}>
                    <SourceSearch ref={sourceRef} />
                </Grid>
                <Grid
                    item
                    xs={12}
                    md={3}
                    sx={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                    }}
                >
                    <Button variant="contained" color="primary" type="submit">
                        Apply Filter
                    </Button>
                    <Button variant="contained" sx={{ mx: 2 }} color="warning" onClick={() => resetFilters()}>
                        Reset Filter
                    </Button>
                </Grid>
            </Grid>
        </form>
    );
};

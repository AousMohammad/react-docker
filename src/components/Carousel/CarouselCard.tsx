import { Box, Typography } from '@mui/material'
import React from 'react'
import { Link } from 'react-router-dom';
import { Datum } from 'types';

type Props = {
    data: Datum;
}

export const CarouselCard: React.FC<Props> = ({ data }) => {
    return (
        <Link to={'/article/' + data.id}>
            <Box sx={{
                width: '100%',
                height: '50vh',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                position: 'relative'
            }}>
                <img style={{ width: '100%', height: '100%', zIndex: 0, position: 'absolute', top: 0, left: 0, filter: 'brightness(0.5)' }} src={data?.image_url ?? 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSKteJt4yIuQOY8dv1-oknm_gx4IRimwZ_rzOT0LY7NiFnd5JsBD8ofiiPhMGeaYpXEr-0&usqp=CAU'} alt="" />
                <Box textAlign='left' width='90%' position='absolute' zIndex='1' left='50%' bottom='20%' sx={{ transform: 'translate(-50%, 50%)' }}>
                    <Typography variant='h5' color='white' fontSize={{ sm: '15px', md: '25px' }}>{data.title}</Typography>
                    <Box display='flex' justifyContent='space-between'>
                        <Typography variant='body1' color='white' fontSize={{ sm: '12px', md: '15px' }}>{data.description}</Typography>
                        <Typography variant='body1' color='white' fontSize={{ sm: '10px', md: '15px' }}>{data.published_at.toString()}</Typography>
                    </Box>
                </Box>
            </Box>
        </Link>
    )
}
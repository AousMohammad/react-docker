import Carousel from 'nuka-carousel'
import React from 'react'
import { CarouselCard } from './CarouselCard'
import { Datum } from 'types';

type Props = {
    data: Datum[] | undefined;
}

export const CCarousel: React.FC<Props> = ({ data }) => {
    return (
        <Carousel autoplay>
            {
                data?.map((item) => <CarouselCard key={item.title} data={item} />)
            }
        </Carousel>
    )
}
import * as React from 'react';
import Card from '@mui/material/Card';

type Props = {
    children: React.ReactElement;
}

export const MyCard: React.FC<Props> = ({ children }) => {
    return (
        <Card sx={{ backgroundColor: '#fff', borderRadius: '15px', p: 5 }}>
            {children}
        </Card>
    );
}
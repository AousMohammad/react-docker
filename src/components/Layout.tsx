import * as React from "react";
import {
    Typography,
    Toolbar,
    Fab,
    List,
    IconButton,
    Drawer,
    Divider,
    CssBaseline,
    Box,
    AppBar,
    Menu,
    MenuItem,
} from "@mui/material";
import {
    AccountCircle,
    KeyboardArrowUp,
    Menu as MenuIcon,
} from "@mui/icons-material";
import { ScrollTop } from "./ScrollTop";
import api from "api";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { State } from "redux/actions";

interface Props {
    children: React.ReactElement;
}

const drawerWidth = 240;

export default function Layout(props: Props) {
    const { children } = props;
    const [mobileOpen, setMobileOpen] = React.useState(false);
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const navigate = useNavigate();

    const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleLogOut = async () => {
        await api.post("logout");
        navigate("/login", { replace: true });
    };

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    const drawer = (
        <Box
            onClick={handleDrawerToggle}
            sx={{
                textAlign: "center",
                padding: 2,
            }}
        >
            <Typography variant="h6">MUI</Typography>
            <Divider />
            <List>
                <MenuItem
                    onClick={() => navigate("/edit-profile", { replace: true })}
                >
                    Edit Profile
                </MenuItem>
                <MenuItem onClick={handleLogOut}>Logout</MenuItem>
            </List>
        </Box>
    );

    const container =
        window !== undefined ? () => window.document.body : undefined;
    const user = useSelector((state: State) => state.user);

    return (
        <Box sx={{ display: "flex" }}>
            <CssBaseline />
            <AppBar
                position="fixed"
                component="nav"
                sx={{ backgroundColor: "#3f50b5", color: "#fff" }}
            >
                <Toolbar sx={{ justifyContent: "space-between", padding: "0 1rem" }}>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        edge="start"
                        onClick={handleDrawerToggle}
                        sx={{ marginInlineStart: 2, display: { sm: "none" } }}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography
                        variant="h6"
                        component="div"
                        sx={{ marginInlineStart: 2, cursor: 'pointer', display: { xs: "none", sm: "block" } }}
                        onClick={() => navigate('/', { replace: true })}
                    >
                        Welcome to my NEWS APP
                    </Typography>
                    <Box display="flex" justifyContent="flex-end" alignItems="center">
                        <Typography>Welcome {user?.name}</Typography>
                        <IconButton
                            size="large"
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={handleMenu}
                            color="inherit"
                        >
                            <AccountCircle />
                        </IconButton>
                        <Menu
                            id="menu-appbar"
                            anchorEl={anchorEl}
                            anchorOrigin={{
                                vertical: "top",
                                horizontal: "right",
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: "top",
                                horizontal: "right",
                            }}
                            open={Boolean(anchorEl)}
                            onClose={handleClose}
                        >
                            <MenuItem
                                onClick={() => navigate("/edit-profile", { replace: true })}
                            >
                                Edit Profile
                            </MenuItem>
                            <MenuItem onClick={handleLogOut}>Logout</MenuItem>
                        </Menu>
                    </Box>
                </Toolbar>
            </AppBar>
            <Box component="nav">
                <Drawer
                    container={container}
                    variant="temporary"
                    open={mobileOpen}
                    onClose={handleDrawerToggle}
                    ModalProps={{
                        keepMounted: true, // Better open performance on mobile.
                    }}
                    sx={{
                        display: { xs: "block", sm: "none" },
                        "& .MuiDrawer-paper": {
                            boxSizing: "border-box",
                            width: drawerWidth,
                        },
                    }}
                >
                    {drawer}
                </Drawer>
            </Box>
            <Box component="main" sx={{ padding: "2rem", flexGrow: 1 }}>
                <Toolbar id="back-to-top-anchor" />
                {children}
            </Box>
            <ScrollTop {...props}>
                <Fab size="small" aria-label="scroll back to top">
                    <KeyboardArrowUp />
                </Fab>
            </ScrollTop>
        </Box>
    );
}

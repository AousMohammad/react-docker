import React, { forwardRef, useImperativeHandle, useState } from 'react';
import { TextField } from '@mui/material';

export const SourceSearch = forwardRef((props, ref) => {
    const [searchText, setSearchText] = useState('');

    const handleAuthorChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const newText = event.target.value;
        setSearchText(newText);
    };

    // Function to get the current input value
    const getData = () => {
        return searchText;
    };

    // Expose the getData function via the ref
    useImperativeHandle(ref, () => ({
        getData,
    }));
    
    return (
        <TextField
            label="Source Search"
            fullWidth
            value={searchText}
            onChange={handleAuthorChange}
            inputRef={ref}
        />
    );
});
import React, { useState, forwardRef, useImperativeHandle } from 'react';
import { TextField } from '@mui/material';

export const AuthorSearch = forwardRef((props, ref) => {
    const [searchText, setSearchText] = useState('');

    const handleAuthorChange = (event: any) => {
        const newText = event.target.value;
        setSearchText(newText);
    };

    // Function to get the current input value
    const getData = () => {
        return searchText;
    };

    // Expose the getData function via the ref
    useImperativeHandle(ref, () => ({
        getData,
    }));

    return (
        <TextField
            label="Author search"
            fullWidth
            value={searchText}
            onChange={handleAuthorChange}
            inputRef={ref}
        />
    );
});

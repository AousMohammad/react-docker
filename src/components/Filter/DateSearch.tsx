import React, { forwardRef, useImperativeHandle, useState } from 'react';
import { DatePicker, LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterMoment } from '@mui/x-date-pickers/AdapterMoment';
import moment from 'moment';

// Define the prop type for the DateSearch component
type DateSearchProps = {
    // Define any props specific to your component here
};

// Define the ref type for the DateSearch component
type DateSearchRef = {
    // Define functions or properties you want to expose via the ref here
    getData: () => Date | null;
};

// Forward the ref to the DatePicker component
export const DateSearch = forwardRef<DateSearchRef, DateSearchProps>((props, ref) => {
    const [dateSearch, setDataSearch] = useState<Date | null>(null);

    const handleDateChange = (date: moment.Moment | null) => {
        // Convert the moment date back to a JavaScript Date before storing it
        setDataSearch(date ? date?.toDate() : null);
    };

    // Function to get the current selected date
    const getData = () => {
        return dateSearch;
    };

    // Expose the getData function via the ref
    useImperativeHandle(ref, () => ({
        getData,
    }));

    return (
        <LocalizationProvider dateAdapter={AdapterMoment}>
            <DatePicker
                sx={{ width: '100%' }}
                label="Date search"
                // Convert the JavaScript Date to a moment date for the DatePicker
                value={dateSearch ? moment(dateSearch) : moment()}
                onChange={handleDateChange}
            />
        </LocalizationProvider>
    );
});

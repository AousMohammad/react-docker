import React from 'react'
import { Divider, Skeleton } from '@mui/material';
import { Datum } from 'types';
import { Link } from 'react-router-dom';

type Props = {
    data: Datum[] | undefined
    loading: boolean;
}

export const SideCard: React.FC<Props> = ({ data, loading }) => {
    return (
        <>
            {
                data?.map((item) =>
                    <div key={item.title}>
                        {
                            loading ? (
                                <>
                                    <Skeleton variant="text" sx={{ fontSize: '1rem' }} />
                                    <Skeleton variant="text" sx={{ fontSize: '1rem' }} />
                                    <Skeleton variant="text" sx={{ fontSize: '1rem' }} />
                                </>
                            ) : (
                                <Link to={'/article/' + item.id} style={{ color: 'black', fontWeight: 'bold', textDecoration: 'none' }}>
                                    {item.title}........ read more
                                </Link>
                            )
                        }
                        <Divider sx={{ my: 6 }} />
                    </div>
                )
            }
        </>
    )
}
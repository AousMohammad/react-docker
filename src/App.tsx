import React from "react";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Outlet,
} from "react-router-dom";
import privateRoutes from "./routes";
import publicRoutes from "./publicRoutes";
import "./styles/style.scss";
import { ThemeProvider, createTheme } from "@mui/material";
import { theme } from "theme/theme";
import { Route as TRoute } from "./types";

// Helper function to render nested routes
const renderRoutes = (routes: TRoute[]) => {
  return routes.map((route) => (
    <Route
      key={route.path}
      path={route.path}
      element={route.element ?? <Outlet />}
    >
      {route.children && renderRoutes(route.children)}
    </Route>
  ));
};

function App() {
  const publicRouteElements = publicRoutes.map((route) => (
    <Route key={route.path} path={route.path} element={route.element} />
  ));

  return (
    <ThemeProvider theme={createTheme(theme)}>
      <Router>
        <Routes>
          {publicRouteElements}
          {renderRoutes(privateRoutes)}
        </Routes>
      </Router>
    </ThemeProvider>
  );
}

export default App;

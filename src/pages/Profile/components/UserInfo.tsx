import { Backdrop, Button, CircularProgress, Grid, TextField } from '@mui/material';
import api from 'api';
import React, { useState } from 'react'
import { useDispatch } from 'react-redux';
import { setUser } from 'redux/actions';
import { toastMessage } from 'utils';

type Props = {
    email: string | undefined;
    name: string | undefined;
    id: number | undefined;
}

export const UserInfo = (props: Props) => {
    const dispatch = useDispatch();
    const [name, setName] = useState(props.name);
    const [email, setEmail] = useState(props.email);
    const [loading, setLoading] = useState(false);
    const saveInfo = (event: any) => {
        setLoading(true);
        event.preventDefault();
        api.put(`/user/info/update`, { email: email, name: name }).then((response) => {
            setLoading(false);
            dispatch(setUser(response.data));
        }).catch(err => {
            setLoading(false);
            toastMessage(err.response.data.message);
        });
    }

    return (
        <form onSubmit={saveInfo}>
            {loading && (<Backdrop
                sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                open={true}
            >
                <CircularProgress color="inherit" />
            </Backdrop>)}
            <Grid container>
                <Grid item xs={12} my={2}>
                    <TextField
                        fullWidth
                        onChange={(e) => setName(e.target.value)}
                        value={name}
                        name='name'
                    />
                </Grid>
                <Grid item xs={12} my={2}>
                    <TextField
                        fullWidth
                        onChange={(e) => setEmail(e.target.value)}
                        value={email}
                        name='email'
                    />
                </Grid>
                <Button type='submit' variant='contained'>
                    Save
                </Button>
            </Grid>
        </form>
    )
}

export default UserInfo
import React, { useState } from 'react';
import { useForm, SubmitHandler, Controller } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { Backdrop, Button, CircularProgress, Grid, TextField } from '@mui/material';
import api from 'api';
import { toastMessage } from 'utils';

interface FormData {
    old_password: string;
    password: string;
    password_confirmation: string;
}

const schema = yup.object().shape({
    old_password: yup.string().required('Old Password is required'),
    password: yup
        .string()
        .min(8, 'Password must be at least 8 characters')
        .required('New Password is required'),
    password_confirmation: yup
        .string()
        .oneOf([yup.ref('password'), undefined], 'Passwords must match')
        .required('Password confirmation is required'),
});


export const UserPassword: React.FC = () => {
    const {
        control,
        handleSubmit,
        formState: { errors },
    } = useForm<FormData>({
        resolver: yupResolver(schema),
    });
    const [loading, setLoading] = useState(false);

    const onSubmit: SubmitHandler<FormData> = (data) => {
        setLoading(true);
        api.put(`/user/password/change`, data).then((response) => {
            setLoading(false);
        }).catch(res => {
            setLoading(false);
            toastMessage(res.response.data);
        });
    };

    return (
        <form onSubmit={handleSubmit(onSubmit)} style={{ width: '100%' }}>
            {loading && (<Backdrop
                sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                open={true}
            >
                <CircularProgress color="inherit" />
            </Backdrop>)}
            <Grid container>
                <Grid item xs={12}>
                    <label htmlFor="old_password">Old Password</label>
                    <Controller
                        name="old_password"
                        control={control}
                        render={({ field }) => <TextField fullWidth {...field} type="password" />}
                    />
                    <p>{errors.old_password?.message}</p>
                </Grid>
                <Grid item xs={12}>
                    <label htmlFor="password">New Password</label>
                    <Controller
                        name="password"
                        control={control}
                        render={({ field }) => <TextField fullWidth {...field} type="password" />}
                    />
                    <p>{errors.password?.message}</p>
                </Grid>
                <Grid item xs={12}>
                <label htmlFor="password_confirmation">Confirm New Password</label>
                    <Controller
                        name="password_confirmation"
                        control={control}
                        render={({ field }) => <TextField fullWidth {...field} type="password" />}
                    />
                    <p>{errors.password_confirmation?.message}</p>
                </Grid>
            </Grid>
            <Button type='submit' variant='contained'>Change Password</Button>
        </form>
    );
};

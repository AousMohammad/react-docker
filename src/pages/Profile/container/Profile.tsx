import React from 'react'
import { useSelector } from 'react-redux'
import { State } from 'redux/actions'
import { MyCard } from 'components'
import { Box, Typography } from '@mui/material'
import { UserInfo, UserPassword } from '../components'
import { AddCategory } from 'pages/AddCategory'

export const Profile = () => {
    const userInfo = useSelector((state: State) => state.user);

    return (
        <>
            <Box my={2}>
                <MyCard>
                    <>
                        <Typography variant='h4'>User information</Typography>
                        <UserInfo name={userInfo?.name} email={userInfo?.email} id={userInfo?.id} />
                    </>
                </MyCard>
            </Box>
            <Box my={2}>
                <MyCard>
                    <>
                        <Typography variant='h4'>User Password</Typography>
                        <UserPassword />
                    </>
                </MyCard>
            </Box>
            <Box my={2}>
                <MyCard>
                    <>
                        <Typography variant='h4'>User Categories</Typography>
                        <AddCategory edit={true} />
                    </>
                </MyCard>
            </Box>
        </>
    )
}
import { AuthContainer } from 'pages/authinication/container';
import React from 'react';
import { LoginForm } from '../components/LoginForm';


export const LoginPage: React.FC = () => {
    return (
        <AuthContainer title='Login' children={<LoginForm />} />
    );
};

export default LoginPage;

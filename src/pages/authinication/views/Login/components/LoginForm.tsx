import React from 'react';
import { useForm } from 'react-hook-form';
import { Alert, Box, Grid, Link } from '@mui/material';
import { FormTextField, SubmitButton } from 'pages/authinication/components';
import { LoginFormInput } from 'types';
import { useSubmit } from 'pages/authinication/hooks';
import Cookies from 'js-cookie';
import { useNavigate } from 'react-router-dom';
import { toastMessage } from 'utils';

export const LoginForm: React.FC = () => {
    const { register, handleSubmit, formState: { errors } } = useForm<LoginFormInput>();
    const { submit, loading, error } = useSubmit<LoginFormInput>('login');
    const navigate = useNavigate();
    const onSubmit = (data: LoginFormInput) => {
        submit(data).then(response => {
            if (response?.message) {
                toastMessage(response?.message);
            } else {
                Cookies.set('token', response?.data.token);
                navigate('/', { replace: true });
            }
        });
    };

    return (
        <Box component="form" onSubmit={handleSubmit(onSubmit)} noValidate sx={{ mt: 1 }}>
            <FormTextField
                register={register("email", {
                    required: "Email is required",
                    pattern: {
                        value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                        message: "Invalid email address"
                    }
                })}
                errors={errors}
                name="email"
                label="Email Address"
                autoFocus
            />
            <FormTextField
                register={register("password", { required: "Password is required", minLength: { value: 8, message: "Password must be at least 8 characters long" } })}
                errors={errors}
                name="password"
                label="Password"
                type="password"
            />
            <Grid container mt={2}>
                <Grid item>
                    <Link href="register" variant="body2">
                        {"Don't have an account? Sign Up"}
                    </Link>
                </Grid>
            </Grid>
            <SubmitButton title='Log in' loading={loading} />
            {
                error && (
                    <Alert severity="error" sx={{ mt: 5, width: '100%' }}>This is an error alert — check it out!</Alert>
                )
            }
        </Box>
    )
}
import React from 'react';
import { useForm } from 'react-hook-form';
import { Alert, Box, Grid, Link } from '@mui/material';
import { RegisterFormInput } from 'types';
import { useSubmit } from 'pages/authinication/hooks';
import { FormTextField, SubmitButton } from 'pages/authinication/components';
import { useNavigate } from 'react-router-dom';
import { toastMessage, toastSuccess } from 'utils';

export const RegisterForm: React.FC = () => {
    const { register, watch, handleSubmit, formState: { errors } } = useForm<RegisterFormInput>();
    const password = watch('password', '');
    const { submit, loading, error } = useSubmit<RegisterFormInput>('register');
    const navigate = useNavigate();

    const onSubmit = (data: RegisterFormInput) => {
        submit(data).then(response => {
            if (response?.message || response?.errors?.length > 0) {
                toastMessage(response?.message || response?.errors[0]);
            } else {
                toastSuccess();
                navigate('/login', { replace: true });
            }
        });
    };

    return (
        <Box component="form" onSubmit={handleSubmit(onSubmit)} noValidate sx={{ mt: 1 }}>
            <FormTextField
                register={register("name", {
                    required: "Name is required",
                    minLength: { value: 5, message: "Name must be at least 5 characters long" }
                })}
                errors={errors}
                name="name"
                label="Name"
                autoFocus
            />
            <FormTextField
                register={register("email", {
                    required: "Email is required",
                    pattern: {
                        value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                        message: "Invalid email address"
                    }
                })}
                errors={errors}
                name="email"
                label="Email Address"
            />
            <FormTextField
                register={register("password", { required: "Password is required", minLength: { value: 8, message: "Password must be at least 8 characters long" } })}
                errors={errors}
                name="password"
                label="Password"
                type="password"
            />
            <FormTextField
                register={register("password_confirmation", {
                    required: "Confirmation password is required",
                    minLength: { value: 8, message: "Confirmation password must be at least 8 characters long" },
                    validate: value =>
                        value === password || "The passwords do not match"
                })}
                errors={errors}
                name="password_confirmation"
                label="Password Confirmation"
                type="password"
            />
            <Grid container mt={2}>
                <Grid item>
                    <Link href="login" variant="body2">
                        {"Do you have an account? Log in"}
                    </Link>
                </Grid>
            </Grid>
            <SubmitButton title='Register' loading={loading} />
            {
                error && (
                    <Alert severity="error" sx={{ mt: 5, width: '100%' }}>This is an error alert — check it out!</Alert>
                )
            }
        </Box>
    )
}

export default RegisterForm;

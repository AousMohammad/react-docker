import { AuthContainer } from 'pages/authinication/container';
import React from 'react';
import { RegisterForm } from '../components';


export const RegisterPage: React.FC = () => {


    return (
        <AuthContainer title="Register" children={<RegisterForm />} />
    );
};

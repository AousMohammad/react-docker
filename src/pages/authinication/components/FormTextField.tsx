import { TextField } from '@mui/material';
import { FormTextFieldProps } from 'types';



export const FormTextField: React.FC<FormTextFieldProps> = ({ register, errors, name, label, type, autoFocus }) => (
    <TextField
        margin="normal"
        required
        fullWidth
        {...register}
        error={Boolean(errors[name])}
        helperText={errors[name]?.message}
        name={name}
        label={label}
        type={type}
        id={name}
        autoComplete={name}
        autoFocus={autoFocus}
    />
);
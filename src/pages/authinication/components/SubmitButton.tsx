import { Send } from '@mui/icons-material'
import { LoadingButton } from '@mui/lab'
import React from 'react'

type Props = {
    loading: boolean;
    title: string;
}

export const SubmitButton: React.FC<Props> = ({ loading, title }) => {
    return (
        <LoadingButton
            color="secondary"
            loading={loading}
            loadingPosition="end"
            endIcon={<Send />}
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
        >
            {title}
        </LoadingButton>
    )
}
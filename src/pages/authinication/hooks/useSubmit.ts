import { useState } from 'react';
import axios from 'axios';

export const useSubmit = <T,>(url: string) => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const submit = (data: T) => {
    setLoading(true);
    return axios.post(process.env.REACT_APP_BACKEND_URL + url, data)
      .then(response => {
        setLoading(false);
        return response;
      })
      .catch(error => {
        setError(true);
        setLoading(false);
        return error.response.data;
      });
  };

  return { submit, loading, error };
}
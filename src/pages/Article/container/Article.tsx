import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { State } from 'redux/actions'
import api from 'api';
import { MyCard } from 'components/MyCard'
import { Datum } from 'types';
import { Divider, Stack, Typography } from '@mui/material';

type Props = {}

export const Article: React.FC = (props: Props) => {
    const { id } = useParams<{ id: string }>()
    const dispatch = useDispatch()
    const articles = useSelector((state: State) => state.articles);
    const [article, setArticle] = useState<Datum>();

    useEffect(() => {
        const fetchArticle = async () => {
            if (!articles || articles.length === 0) {
                const result = await api.get(`article/${id}`);
                setArticle(result.data.data);
            } else {
                setArticle(articles.filter(item => item.id.toString() === id)[0]);
            }
        }
        fetchArticle()
    }, [articles, dispatch, id])

    return (
        <MyCard>
            <>
                <img src={article?.image_url} alt="" style={{ width: '100%', height: '50vh', objectFit: 'cover' }} />
                <Stack direction='row' justifyContent='space-between'>
                    {article?.author?.name && (<Typography>Author: {article.author.name}</Typography>)}
                    {article?.published_at && (<Typography>Published at: {article?.published_at.toString()}</Typography>)}
                    {article?.source?.name && (<Typography>Author: {article.source.name}</Typography>)}
                    {article?.category?.name && (<Typography>Category: {article.category.name}</Typography>)}
                </Stack>
                <Divider sx={{ my: 5 }} />
                <Typography mb={5} variant='h4' fontSize={{ xs: '25px'}}>{article?.title}</Typography>
                <Typography>{article?.description}</Typography>
            </>
        </MyCard>
    )
}

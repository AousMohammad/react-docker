import React, { useEffect, useState } from 'react';
import api from 'api';
import { useDispatch, useSelector } from 'react-redux';
import { Dispatch, State, ThunkDispatch, setCategories, setUser } from 'redux/actions';
import { Category, User } from 'types';
import { LoadingButton } from '@mui/lab';
import { Save } from '@mui/icons-material';
import { useNavigate } from 'react-router-dom';
import { PageSkeleton } from 'components';
import { Box, Button, Typography } from '@mui/material';
import { CategoryCard } from '../components';
import { toastMessage, toastSuccess } from 'utils';

type Props = {
    edit?: boolean;
};

export const AddCategory: React.FC<Props> = ({ edit }) => {
    const categories = useSelector((state: State) => state.categories);
    const userCategory = useSelector((state: State) => state.user);
    const [selectedCards, setSelectedCards] = useState<number[]>(
        edit ? userCategory?.preferred_categories.map((item: any) => item.id) : []
    );
    const dispatch = useDispatch<ThunkDispatch>();
    const dispatcher = useDispatch();
    const navigate = useNavigate();
    const [showSkeleton, setShowSkeleton] = useState(false);

    useEffect(() => {
        if (categories.length === 0) {
            setShowSkeleton(true);
            dispatch(fetchCategories()).then((res) => {
                if (res) {
                    setShowSkeleton(false);
                } else {
                    toastMessage("There's something wrong");
                }
            });
        }
    }, [dispatch, categories.length]);

    const handleCardClick = (id: number) => {
        setSelectedCards((prevSelectedCards) =>
            prevSelectedCards.includes(id)
                ? prevSelectedCards.filter((cardId) => cardId !== id)
                : [...prevSelectedCards, id]
        );
    };

    const [loading, setLoading] = React.useState(false);

    const handleSave = () => {
        setLoading(true);
        setShowSkeleton(true);

        const apiEndpoint = edit ? '/user/categories/change' : '/add_preferences';
        const requestData = edit ? { ids: [...selectedCards] } : { ids: selectedCards };

        api
            .post(apiEndpoint, requestData)
            .then(async (response) => {
                toastSuccess();
                if (edit) {
                    const userData = await api.get('checkValid');
                    if (userData.status === 200) {
                        const data = userData.data.user as User;
                        dispatcher(setUser(data));
                    }
                }
                setLoading(false);
                setShowSkeleton(false);
                navigate('/', { replace: true });
            })
            .catch((err) => {
                setLoading(false);
                setShowSkeleton(false);
                toastMessage(err.response.message);
            });
    };

    return (
        <>
            {showSkeleton && <PageSkeleton />}
            {edit && (
                <Box my={6} textAlign="center">
                    <Typography variant="h4" mb={2} fontSize={{ xs: '22px', md: '30px' }}>
                        Please select your preferred categories (at least 1)
                    </Typography>
                </Box>
            )}
            <Box
                display="flex"
                flexDirection="row"
                justifyContent="space-around"
                flexWrap="wrap"
                p={2}
                m={2}
                gap={2}
            >
                {(categories as any)?.categories?.map((card: Category) => (
                    <CategoryCard
                        key={card.id}
                        id={card.id}
                        name={card.name}
                        selected={selectedCards.includes(card.id)}
                        onClick={() => handleCardClick(card.id)}
                    />
                ))}
                {edit ? (
                    <Box display="flex" justifyContent="center" alignItems="center">
                        <Button variant="contained" color="secondary" onClick={handleSave}>
                            Save
                        </Button>
                    </Box>
                ) : (
                    <Box
                        sx={{
                            position: 'fixed',
                            bottom: '0',
                            left: '0',
                            backgroundColor: 'white',
                            width: '100%',
                            height: '10vh',
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}
                    >
                        <LoadingButton
                            color="primary"
                            onClick={handleSave}
                            loading={loading}
                            loadingPosition="start"
                            disabled={selectedCards.length === 0}
                            startIcon={<Save />}
                            variant="contained"
                        >
                            <span>Save</span>
                        </LoadingButton>
                    </Box>
                )}
            </Box>
        </>
    );
}


export function fetchCategories() {
    return async (dispatch: Dispatch) => {
        try {
            const response = await api.get('categories');
            dispatch(setCategories(response.data));
            return true;
        } catch (error) {
            return error;
        }
    };
}
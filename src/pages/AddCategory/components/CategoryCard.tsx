import { Box, Card, CardContent, Typography } from "@mui/material";

export const CategoryCard: React.FC<{
    id: number;
    name: string;
    selected: boolean;
    onClick: () => void;
}> = ({ id, name, selected, onClick }) => (
    <Box
        key={id}
        onClick={onClick}
        sx={{
            p: 2,
            width: { xs: '100%', md: 300 },
            cursor: 'pointer',
            borderRadius: '12px',
            '&:hover': {
                boxShadow: 3,
                transition: 'all 0.3s ease',
            },
        }}
    >
        <Card
            sx={{
                bgcolor: selected ? 'primary.main' : 'background.paper',
                color: selected ? 'white' : 'text.primary',
                borderRadius: '8px',
            }}
        >
            <CardContent>
                <Typography variant="h5" component="div">
                    {name}
                </Typography>
            </CardContent>
        </Card>
    </Box>
);
import React from 'react';
import { Divider, Grid, Typography } from "@mui/material";

interface SectionProps {
    title: string;
    loading: boolean;
    articles: any[];
    children: React.ReactElement;
    md?: number;
    sm?: number;
}

export const Section: React.FC<SectionProps> = ({ title, loading, articles, children, md, sm }) => (
    <>
        <Grid item xs={sm ? 3 : md ? 8 : 12} mt={5}>
            <Divider textAlign="left" sx={{ mb: 2 }}>
                <Typography variant='h4' color="textPrimary" mb={2}>
                    {title}
                </Typography>
            </Divider>
            {React.cloneElement(children, { loading, articles })}
        </Grid>
    </>
);

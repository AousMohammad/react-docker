import { Pagination } from "@mui/material";

export const HomePagination: React.FC<{ pageCount: number | undefined; onPageChange: (page: number) => void }> = ({ pageCount, onPageChange }) => (
    <Pagination
      count={pageCount}
      sx={{ width: '100%', "& ul": { justifyContent: 'center', mt: 5 } }}
      onChange={(event, page) => onPageChange(page)}
      color="primary"
    />
  );
import React, { useEffect, useState } from 'react';
import { Box, Divider, Grid, Typography } from '@mui/material';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { useDataLoad } from '../hooks';
import { CCarousel, MyCard, NewsCard, PageSkeleton, SearchAndFilter, SideCard } from 'components';
import { HomePagination, Section } from '../components';

const SearchAndFilterSection = () => (
    <Grid item xs={12}>
        <Divider>
            <Typography variant='h4' my={2}>
                Search and Filter
            </Typography>
        </Divider>
        <Grid item xs={12} my={2} p={2} sx={{ boxShadow: "0px 0px 5px 0px #3b3b3b", background: "white" }}>
            <SearchAndFilter />
        </Grid>
    </Grid>
);

const VerticalDivider = () => (
    <Grid item md={1} mt={5} display='flex' justifyContent='center' alignItems='center'>
        <Divider orientation="vertical" />
    </Grid>
);


export const Home: React.FC = () => {
    const [page, setPage] = useState<number | undefined>(undefined);
    const authorSearch = useSelector((state: any) => state.authorSearch);
    const dateSearch = useSelector((state: any) => state.dateSearch);
    const sourceSearch = useSelector((state: any) => state.sourceSearch);
    const user = useSelector((state: any) => state.user);
    const { data, loading } = useDataLoad(page ?? 1, authorSearch || null, dateSearch || null, sourceSearch || null);
    const [showSkeleton, setShowSkeleton] = useState(true);
    const navigate = useNavigate();

    useEffect(() => {
        if (!loading && data?.articles.data.length === 0 && user.preferred_categories.length === 0) {
            navigate('/add-category', { replace: true });
        }
    }, [loading, data?.articles.data.length, user.preferred_categories.length, navigate]);

    useEffect(() => {
        if (loading) {
            setShowSkeleton(true);
        } else {
            setShowSkeleton(true);
            const hideSkeletonTimeout = setTimeout(() => {
                setShowSkeleton(false);
            }, 1000);

            return () => clearTimeout(hideSkeletonTimeout);
        }
    }, [loading]);

    return (
        <Box my={5}>
            {showSkeleton && <PageSkeleton />}
            <MyCard>
                <Grid container spacing={4}>
                    <Section title="Latest News .." loading={showSkeleton} articles={data?.articles.data ?? []}>
                        <CCarousel data={data?.articles.data.slice(0, 6)} />
                    </Section>
                    <SearchAndFilterSection />
                    <Section md={9} title="More News" loading={showSkeleton} articles={data?.articles.data ?? []}>
                        <NewsCard loading={loading} data={data?.articles.data.slice(12)} />
                    </Section>
                    <VerticalDivider />
                    <Section sm={3} title="Visit Articles .." loading={showSkeleton} articles={data?.articles.data ?? []}>
                        <SideCard loading={loading} data={data?.articles.data.slice(6, 12)} />
                    </Section>
                    {!loading && data?.articles.data.length === 0 && <>There's No Data</>}
                    <HomePagination pageCount={data?.articles.last_page} onPageChange={(newPage) => setPage(newPage)} />
                </Grid>
            </MyCard>
        </Box>
    );
};

export default Home;

import React from "react";
import api from "api";
import { Articles } from "types";
import Cookies from "js-cookie";
import { useDispatch } from "react-redux";
import { setArticles } from "redux/actions";

export const useDataLoad = (page: number, author: string | any, date: Date | any, source: string | any) => {
    const [data, setData] = React.useState<Articles>();
    const [loading, setLoading] = React.useState<boolean>(true);
    const [error, setError] = React.useState<Error | null>(null);
    const dispatch = useDispatch();
    
    const loadArticles = async () => {
        setLoading(true);
        try {
            const params: any = { page }; // Initialize the params with the page

            // Add author, date, and source to the params only if they have values
            if (author !== null) {
                params.author = author;
            }
            if (date !== null) {
                params.date = date;
            }
            if (source !== null) {
                params.source = source;
            }
            const response = await api.get("articles", {
                headers: {
                    'Authorization': 'Bearer ' + Cookies.get('token')
                  },
                params
            });
            dispatch(setArticles(response.data.articles.data));
            setData(response.data);
            setError(null);
        } catch (error: any) {
            setError(error);
        } finally {
            setLoading(false);
        }
    };

    React.useEffect(() => {
        loadArticles();
        // eslint-disable-next-line
    }, [page, author, date, source]);

    return { data, loading, error }
}

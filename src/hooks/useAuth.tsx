import { useEffect, useState } from "react";
import api from "api";
import { useDispatch } from "react-redux";
import { setUser } from "redux/actions";
import { useNavigate } from "react-router-dom";

export function useAuth() {
    const [auth, setAuth] = useState<boolean | null>(null);
    const [loading, setLoading] = useState(true);
    const dispatch = useDispatch();
    const navigate = useNavigate();
    useEffect(() => {
        const checkAuthentication = async () => {
            try {
                const response = await api.get("checkValid");
                if (response.status === 200) {
                    setAuth(true);
                    dispatch(setUser(response.data.user));  // store the user data in Redux store
                    if (response.data.user.preferred_categories.length === 0) {
                        navigate('/add-category', { replace: true});
                    }
                } else {
                    setAuth(false);
                    navigate('/login', { replace: true });
                }
            } catch (error) {
                setAuth(false);
                navigate('/login', { replace: true });
            } finally {
                setLoading(false);
            }
        };
        checkAuthentication();
    }, [dispatch]);  // add dispatch to the dependencies array

    return { auth, loading };
}

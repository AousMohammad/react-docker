import { LoginPage, RegisterPage } from "./pages";
import { publicRoute } from "./types";

const publicRoutes: publicRoute[] = [
    {
        path: 'login',
        element: <LoginPage />,
    },
    {
        path: 'register',
        element: <RegisterPage />,
    },
    {
        path: 'forget-password',
        // element: <ForgetPasswordPage />,
    },
    {
        path: 'reset-password',
        // element: <ResetPasswordPage />,
    },
];

export default publicRoutes;

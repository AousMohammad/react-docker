import { ReactElement } from "react";
import { UseFormRegisterReturn } from "react-hook-form";

export type Route = {
    path: string;
    element?: ReactElement;
    name?: string;
    icon?: ReactElement;
    children?: Route[];
}
export type publicRoute = {
    path: string;
    element?: ReactElement;
    children?: publicRoute[];
}

export type LoginFormInput = {
    email: string;
    password: string;
}
export type RegisterFormInput = {
    email: string;
    name: string;
    password: string;
    password_confirmation: string;
}

export type Articles = {
    articles: ArticlesClass;
}

export type ArticlesClass = {
    current_page:   number;
    data:           Datum[];
    first_page_url: string;
    from:           number;
    last_page:      number;
    last_page_url:  string;
    links:          Link[];
    next_page_url:  string;
    path:           string;
    per_page:       number;
    prev_page_url:  string;
    to:             number;
    total:          number;
}

export type Datum = {
    id:           number;
    title:        string;
    description:  string | undefined;
    image_url:    string | undefined;
    article_url:  string;
    published_at: Date;
    source_id:    number;
    category_id:  number;
    author_id:    number | null;
    created_at:   Date;
    updated_at:   Date;
    author:       Category | null;
    source:       Category;
    category:     Category;
}

export type Category = {
    id:         number;
    name:       string;
    created_at: Date | null;
    updated_at: Date | null;
}

export type Link = {
    url:    null | string;
    label:  string;
    active: boolean;
}


export type FormTextFieldProps = {
    register: UseFormRegisterReturn;
    errors: Record<string, any>;
    name: string;
    label: string;
    type?: string;
    autoFocus?: boolean;
}


export type Categories = {
    categories: Category[];
}


export type UserValid = {
    msg:  string;
    user: User;
}

export type User = {
    id:                number;
    name:              string;
    email:             string;
    email_verified_at: null;
    created_at:        Date;
    updated_at:        Date;
    preferred_categories: any;
}

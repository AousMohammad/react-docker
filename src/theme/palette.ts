import { PaletteOptions, PaletteColorOptions } from "@mui/material";

const primary: PaletteColorOptions = {
  main: "#4CAF50",
  light: "#81C784",
  dark: "#388E3C",
  contrastText: "#FFF",
};

const secondary: PaletteColorOptions = {
  main: "#FF5722",
  light: "#FF8A65",
  dark: "#E64A19",
};

export const palette: PaletteOptions = {
  primary,
  secondary,
  common: {
    black: "#424242",
    white: "#FAFAFA",
  },
  background: {
    default: "#F5F5F5",
  },
};

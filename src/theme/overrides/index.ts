import { Components, Theme } from "@mui/material";
import { ButtonStylesOverrides } from "./Button";
import { CssBaselineStylesOverrides } from "./Cssbaseline";
import { DialogStylesOverrides } from "./Dialog";
import { MenuStylesOverrides } from "./Menu";
import { MenuItemStylesOverrides } from "./MenuItem";
import { DialogTitleStylesOverrides } from "./DialogTitle";

export const componentsOverrides: Components<Theme> = {
  MuiButton: ButtonStylesOverrides,
  MuiMenu: MenuStylesOverrides,
  MuiMenuItem: MenuItemStylesOverrides,
  MuiCssBaseline: CssBaselineStylesOverrides,
  MuiDialog: DialogStylesOverrides,
  MuiDialogTitle: DialogTitleStylesOverrides,
};

import { TypographyOptions } from "@mui/material/styles/createTypography";

const typography: TypographyOptions = {
  fontFamily: "Cairo",
  fontWeightRegular: 400,
  fontWeightMedium: 500,
  fontWeightBold: 700,
  allVariants: {
    letterSpacing: 0,
    fontWeight: 500,
  },
  h1: {
    fontFamily: "Cairo",
    fontSize: "2.5rem",
    fontWeight: 700,
    "@media (max-width: 600px)": {
      fontSize: "2rem",
    },
  },
  h2: {
    fontFamily: "Cairo",
    fontSize: "2rem",
    fontWeight: 700,
    "@media (max-width: 600px)": {
      fontSize: "1.75rem",
    },
  },
  h3: {
    fontFamily: "Cairo",
    fontSize: "1.75rem",
    fontWeight: 700,
    "@media (max-width: 600px)": {
      fontSize: "1.5rem",
    },
  },
  h4: {
    fontFamily: "Cairo",
    fontSize: "1.5rem",
    fontWeight: 700,
    "@media (max-width: 600px)": {
      fontSize: "1.25rem",
    },
  },
  h5: {
    fontFamily: "Cairo",
    fontSize: "1.25rem",
    fontWeight: 700,
    "@media (max-width: 600px)": {
      fontSize: "1.125rem",
    },
  },
  h6: {
    fontFamily: "Cairo",
    fontSize: "1rem",
    fontWeight: 700,
    "@media (max-width: 600px)": {
      fontSize: "0.875rem",
    },
  },
  body1: {
    fontFamily: "Cairo",
    fontSize: "1rem",
    fontWeight: 400,
    "@media (max-width: 600px)": {
      fontSize: "0.875rem",
    },
  },
  body2: {
    fontFamily: "Cairo",
    fontSize: "0.875rem",
    fontWeight: 400,
    "@media (max-width: 600px)": {
      fontSize: "0.75rem",
    },
  },
  button: {
    fontFamily: "Cairo",
    fontWeight: 500,
    fontSize: "0.75rem",
    "@media (max-width: 600px)": {
      fontSize: "0.625rem",
    },
  },
};

export default typography;

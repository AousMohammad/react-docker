import { PrivateRoute } from "./components";
import { AddCategory, Article, Home, Profile } from "./pages";
import { Route } from "./types";



const routes: Route[] = [
  {
    path: '/',
    element: <PrivateRoute />,
    children: [
      {
        path: '',
        name: 'home',
        element: <Home />
      },
      {
        path: 'article/:id',
        name: 'Article',
        element: <Article />
      },
      {
        path: 'add-category',
        name: 'add-category',
        element: <AddCategory />
      },
      {
        path: 'edit-profile',
        name: 'edit-profile',
        element: <Profile />
      },
    ]
  }
];

export default routes;

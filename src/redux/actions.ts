import { Category, User, Datum } from "types";
import { ThunkDispatch as OriginalThunkDispatch } from 'redux-thunk';

// Action types
export type Action =
| { type: 'SET_FILTERS', payload: string[] }
| { type: 'SET_SEARCH', payload: string }
| { type: 'SET_AUTHOR_SEARCH', payload: string }
| { type: 'SET_DATE_SEARCH', payload: Date | null }
| { type: 'SET_SOURCE_SEARCH', payload: string }
| { type: 'SET_CATEGORIES', payload: Category[] }
| { type: 'SET_ARTICLES', payload: Datum[] }
| { type: 'SET_USER', payload: User };

  export type ThunkDispatch = OriginalThunkDispatch<State, undefined, Action>;
  export type ThunkAction = (dispatch: Dispatch) => Promise<void>;

  export type Dispatch = (action: Action | ((dispatch: Dispatch) => Promise<void>)) => void;
  

// Your Redux state type
export interface State {
  filters: string[];
  search: string;
  authorSearch: string;
  dateSearch: Date | null;
  sourceSearch: string;
  categories: Category[];
  user: User | null;
  articles: Datum[] | null
}

export function setFilters(filters: string[]) {
  return {
    type: 'SET_FILTERS',
    payload: filters,
  };
}

export function setSearch(search: string) {
  return {
    type: 'SET_SEARCH',
    payload: search,
  };
}

export function setAuthorSearch(author: string) {
  return {
    type: 'SET_AUTHOR_SEARCH',
    payload: author,
  };
}

export function setDateSearch(date: string | null) {
  return {
    type: 'SET_DATE_SEARCH',
    payload: date,
  };
}

export function setSourceSearch(source: string) {
  return {
    type: 'SET_SOURCE_SEARCH',
    payload: source,
  };
}

export function setCategories(categories: Category[]) {
  return {
    type: 'SET_CATEGORIES' as const,
    payload: categories,
  };
}

export function setUser(user: User) {
  return {
    type: 'SET_USER',
    payload: user,
  };
}

export function setArticles(articles: Datum[]) {
  return {
    type: 'SET_ARTICLES',
    payload: articles,
  };
}
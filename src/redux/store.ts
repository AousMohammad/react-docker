import { createStore, applyMiddleware, Store as ReduxStore } from 'redux';
import thunk from 'redux-thunk';
import { State, Action, Dispatch } from './actions';

const initialState: State = {
  filters: [],
  search: '',
  authorSearch: '',
  dateSearch: null,
  sourceSearch: '',
  categories: [],
  user: null,
  articles: []
};

function reducer(state = initialState, action: Action) {
  switch (action.type) {
    case 'SET_FILTERS':
      return { ...state, filters: action.payload };
    case 'SET_SEARCH':
      return { ...state, search: action.payload };
    case 'SET_AUTHOR_SEARCH':
      return { ...state, authorSearch: action.payload };
    case 'SET_DATE_SEARCH':
      return { ...state, dateSearch: action.payload };
    case 'SET_SOURCE_SEARCH':
      return { ...state, sourceSearch: action.payload };
    case 'SET_CATEGORIES':
      return { ...state, categories: action.payload };
    case 'SET_USER':
      return { ...state, user: action.payload };
    case 'SET_ARTICLES':
        return { ...state, articles: action.payload };
    default:
      return state;
  }
}

export const store: ReduxStore<State, Action> & { dispatch: Dispatch } = createStore(reducer, applyMiddleware(thunk));
